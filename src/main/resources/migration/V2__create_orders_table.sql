CREATE TABLE IF NOT EXISTS orders
(
    id          BIGINT AUTO_INCREMENT,
    product_id bigint not null,
    numbers INT,
    PRIMARY KEY (id)
);

