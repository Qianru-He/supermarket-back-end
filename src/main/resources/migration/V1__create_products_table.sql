CREATE TABLE IF NOT EXISTS products
(
    id    BIGINT AUTO_INCREMENT,
    name  VARCHAR(128) NOT NULL,
    price Decimal      NOT NULL,
    unit  varchar(20)  not null,
    image varchar(512) not null,
    PRIMARY KEY (id)
);