package com.twuc.webApp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "products")
@NotNull
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 128)
    private String name;
    @Column(length = 10)
    private Double price;
    @Column(length = 20)
    private String unit;
    @Column(length = 512)
    private String image;

    public Product() {
    }

    public Product(String name, Double price, String unit, String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public String getName() {
        return this.name;
    }

    public Long getId() {
        return id;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
