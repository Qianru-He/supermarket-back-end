package com.twuc.webApp.constract;

import javax.validation.constraints.NotNull;

public class CreateProductRequest {
    @NotNull
    private String name;
    @NotNull
    private Double price;
    @NotNull
    private String unit;
    @NotNull
    private String image;

    public CreateProductRequest(@NotNull String name, @NotNull Double price, @NotNull String unit, @NotNull String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public CreateProductRequest(@NotNull String name, @NotNull Double price) {
        this.name = name;
        this.price = price;
    }

    public CreateProductRequest() {
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
