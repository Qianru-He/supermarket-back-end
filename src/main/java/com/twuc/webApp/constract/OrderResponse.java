package com.twuc.webApp.constract;

import com.twuc.webApp.domain.Product;

public class OrderResponse {
    private Long id;
    private int numbers;
    private Product product;

    public OrderResponse(Long id,int numbers, Product product) {
        this.id = id;
        this.numbers = numbers;
        this.product = product;
    }

    public OrderResponse() {
    }

    public int getNumbers() {
        return numbers;
    }

    public Product getProduct() {
        return product;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
