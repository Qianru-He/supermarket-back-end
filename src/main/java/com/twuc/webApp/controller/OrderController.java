package com.twuc.webApp.controller;

import com.twuc.webApp.constract.OrderResponse;
import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.OrderRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {
    private ProductRepository productRepository;

    private OrderRepository orderRepository;

    public OrderController(ProductRepository productRepository, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    @PostMapping("/{id}")
    public ResponseEntity createOrder(@PathVariable Long id){
        Product product = productRepository.findById(id).get();

        Optional<Order> order = orderRepository.findByProduct(product);

        if(order.isPresent()){
            Order saveOrder = order.get();
            saveOrder.setNumbers(saveOrder.getNumbers()+1);
            orderRepository.saveAndFlush(saveOrder);
            return ResponseEntity.status(204).build();
        }else {
            Order newOrder = new Order(product,1);
            orderRepository.saveAndFlush(newOrder);
            return ResponseEntity.status(201).build();
        }
    }

    @GetMapping
    public ResponseEntity getAllOrders(){
        List<Order> orders = orderRepository.findAll();
        List<OrderResponse> orderResponses = new ArrayList<>();
        for (Order order : orders) {
            OrderResponse orderResponse;
            orderResponse = new OrderResponse(order.getId(),order.getNumbers(), order.getProduct());
            orderResponses.add(orderResponse);
        }
        return ResponseEntity.status(200).body(orderResponses);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrderById(@PathVariable Long id){
        orderRepository.deleteById(id);

        return ResponseEntity.status(200)
                .build();
    }
}
