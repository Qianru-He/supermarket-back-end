package com.twuc.webApp.controller;

import com.twuc.webApp.constract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {
    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest){
        Optional<Product> findProduct = productRepository.findByName(createProductRequest.getName());
        if(findProduct.isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("此商品已存在！");
        }
        Product product = new Product(createProductRequest.getName(), createProductRequest.getPrice(), createProductRequest.getUnit(), createProductRequest.getImage());
        productRepository.save(product);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping
    public ResponseEntity getProducts(){
        List<Product> products = productRepository.findAll();
        return ResponseEntity.status(200).body(products);
    }
}
