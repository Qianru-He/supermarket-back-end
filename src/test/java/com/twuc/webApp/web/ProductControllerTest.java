package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.constract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;

    @Test
    void should_return_201_when_create_product() throws Exception {
        CreateProductRequest product = new CreateProductRequest("可乐", 5.00, "瓶",
                "https://www.coca-cola.hk/content/dam/journey/hk/zh/private/brands/coca-cola/3_Coca-Cola_Original_Taste_596x334.png");

        mockMvc.perform(post("/api/products")
                .content(new ObjectMapper().writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(201));
    }

    @Test
    void should_return_400_when_product_info_incomplete() throws Exception {
        CreateProductRequest productRequest = new CreateProductRequest("可乐", 5.00);
        mockMvc.perform(post("/api/products")
                .content(new ObjectMapper().writeValueAsString(productRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_save_an_already_exist_product() throws Exception {
        CreateProductRequest alreadyExistProduct = new CreateProductRequest("可乐", 5.00, "瓶",
                "https://www.coca-cola.hk/content/dam/journey/hk/zh/private/brands/coca-cola/3_Coca-Cola_Original_Taste_596x334.png");
        Product product = new Product(alreadyExistProduct.getName(), alreadyExistProduct.getPrice(), alreadyExistProduct.getUnit(), alreadyExistProduct.getImage());
        productRepository.saveAndFlush(product);

        mockMvc.perform(post("/api/products")
                .content(new ObjectMapper().writeValueAsString(alreadyExistProduct))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(400));
    }


    @Test
    void should_return_200_when_get_products() throws Exception {
        Product product = new Product("cola", 3.00, "bottle",
                "https://www.coca-cola.hk/content/dam/journey/hk/zh/private/brands/coca-cola/3_Coca-Cola_Original_Taste_596x334.png");
        productRepository.save(product);
        mockMvc.perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("cola"))
                .andExpect(jsonPath("$[0].price").value(3.00))
                .andExpect(jsonPath("$[0].unit").value("bottle"))
                .andExpect(jsonPath("$[0].image").value("https://www.coca-cola.hk/content/dam/journey/hk/zh/private/brands/coca-cola/3_Coca-Cola_Original_Taste_596x334.png"));
    }

}
