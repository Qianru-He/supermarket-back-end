package com.twuc.webApp.web;

import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.OrderRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Test
    void should_return_201_when_create_a_new_order() throws Exception {
        Product product = new Product("apple", 5d, "jin",
                "http://image.tianjimedia.com/uploadImages/2016/066/32/1GD0NH46QZBQ.jpg");
        productRepository.saveAndFlush(product);

        mockMvc.perform(post("/api/orders/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(201));
    }

    @Test
    void should_return_204_when_create_already_exist_order() throws Exception {
        Product product = new Product("apple", 5d, "jin",
                "http://image.tianjimedia.com/uploadImages/2016/066/32/1GD0NH46QZBQ.jpg");
        productRepository.saveAndFlush(product);

        mockMvc.perform(post("/api/orders/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(201));
        mockMvc.perform(post("/api/orders/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(204));
    }

    @Test
    void should_return_200_when_get_orders() throws Exception {
        Product products = new Product("apple", 5d, "jin",
                "http://image.tianjimedia.com/uploadImages/2016/066/32/1GD0NH46QZBQ.jpg");
        productRepository.saveAndFlush(products);
        Order orders = new Order(products, 2);
        orderRepository.saveAndFlush(orders);

        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].numbers").value(2));
    }

    @Test
    void should_return_200_when_delete_order() throws Exception {
        Product product = new Product("apple", 5d, "jin",
                "http://image.tianjimedia.com/uploadImages/2016/066/32/1GD0NH46QZBQ.jpg");
        Product savedProduct = productRepository.saveAndFlush(product);

        Order order = new Order(savedProduct, 1);
        Order savedOrder = orderRepository.saveAndFlush(order);

        mockMvc.perform(delete("/api/orders/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200));

    }
}